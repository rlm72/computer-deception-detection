"""
A script that extracts features (currently just AUs) from the clips.
"""
import os
import subprocess
from multiprocessing import Pool

from deception.constants import OPENFACE_LOCATION, DATASET_LOCATION, FEATURES_LOCATION, CLIP_COUNTS
from deception.utils import clip_filename


def extract_features(category, number):
    """
    Extract AUs for the nth clip in either the deceptive or truthful category.
    Output is a CSV in the processed directory.
    """
    filename = clip_filename(category, number)
    file_path = os.path.join(DATASET_LOCATION, 'Clips', category, filename)
    script_path = os.path.expanduser(os.path.join(OPENFACE_LOCATION, 'FeatureExtraction'))
    subprocess.run([script_path, '-f', file_path, '-out_dir', FEATURES_LOCATION, '-aus'], stdout=subprocess.DEVNULL)
    print(category, number)


if __name__ == '__main__':
    results = []
    with Pool(processes=4) as pool:
        for category in ('Deceptive', 'Truthful'):
            for i in range(1, CLIP_COUNTS[category] + 1):
                results.append(pool.apply_async(extract_features, [category, i]))
        for result in results:
            result.wait()
