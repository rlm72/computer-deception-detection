"""
Find overlaps between intervals efficiently, using centred interval trees.

Used to find overlapping expressions.
"""
from collections import namedtuple, defaultdict
from itertools import chain

Interval = namedtuple('Interval', 'key start end')        # key represents the AU an interval comes from
ITreeNode = namedtuple('ITreeNode', 'x left right start_sorted end_sorted')

def build_tree(intervals):
    """
    Construct a centred interval tree.

    An interval tree is either an `ITreeNode` instance or `None`.
    """
    if not intervals:
        return None

    x = intervals[len(intervals) // 2].start

    left_intervals = []
    centre_intervals = []
    right_intervals = []
    for i in intervals:
        if i.end <= x:
            left_intervals.append(i)
        elif i.start > x:
            right_intervals.append(i)
        else:
            centre_intervals.append(i)

    left = build_tree(left_intervals)
    right = build_tree(right_intervals)
    start_sorted = sorted(centre_intervals, key=lambda x: x.start)
    end_sorted = sorted(centre_intervals, key=lambda x: x.end, reverse=True)

    return ITreeNode(x, left, right, start_sorted, end_sorted)


def find_intersections(point, tree):
    """
    Find all intervals in a tree that contain a point.
    """
    if not tree:
        return []

    result = []
    if point < tree.x:
        for interval in tree.start_sorted:
            if interval.start > point:
                break
            result.append(interval)

        result.extend(find_intersections(point, tree.left))

    elif point > tree.x:
        for interval in tree.end_sorted:
            if interval.end <= point:
                break
            result.append(interval)

        result.extend(find_intersections(point, tree.right))

    else:
        # intersecting intervals are exactly those at this node
        result = tree.start_sorted

    return result


def all_intersections(interval_lists):
    """
    Find all intersections between pairs of intervals.

    Values in each list should be disjoint (since they come from the same AU).
    There's no need to return each intersection pair in both orders,
    so build the tree and find the intersections with each left endpoint of each interval.
    """
    result = defaultdict(list)

    all_intervals = list(chain.from_iterable(interval_lists))
    tree = build_tree(all_intervals)

    for interval in all_intervals:
        intersections = find_intersections(interval.start, tree)
        for other in intersections:
            pair = ((interval.start, interval.end), (interval.start, interval.end))
            if interval.key < other.key:
                result['{}_{}'.format(interval.key, other.key)].append(pair)
            elif other.key < interval.key:
                if interval.start != other.start:
                    # avoid double counting when starts are equal
                    result['{}_{}'.format(other.key, interval.key)].append(pair)

            # ignore the one intersection of the point with itself

    return result
