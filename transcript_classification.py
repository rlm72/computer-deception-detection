"""
Classifies videos based on features from transcripts.
"""
import os

import numpy as np
import pandas as pd
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.feature_selection import GenericUnivariateSelect
from sklearn.svm import SVC
from sklearn.metrics import roc_auc_score

from constants import CLIP_COUNTS, UNUSED_COUNTS, TRANSCRIPTS_LOCATION
from utils import all_filenames, get_subjects, StratifiedGroupKFold

filenames = []
for f in all_filenames('txt'):
    if 'lie' in f:
        category = 'Deceptive'
    elif 'truth' in f:
        category = 'Truthful'
    else:
        raise ValueError('Unknown sample class')

    filenames.append(os.path.join(TRANSCRIPTS_LOCATION, category, f))

filenames = np.array(filenames)
Y = pd.Series([1] * (CLIP_COUNTS['Deceptive'] - UNUSED_COUNTS['Deceptive']) + [0] * (CLIP_COUNTS['Truthful'] - UNUSED_COUNTS['Truthful']))
groups = get_subjects()


"""
reg_exponents = range(1, 8, 1)
gamma_exponents = range(-5, 3, 1)
n_features = range(15, 25, 1)
reg_exponents = range(3, 15, 1)
gamma_exponents = range(-6, 2, 1)
n_features = range(17, 40, 1)
"""


def do_classification(hyperparameters):
    """
    Perform a round of cross-validation and report the results.

    `hyperparameters` should be a list of `n_features, reg_exponent, gamma_exponent` features.
    """
    results = {}

    for index, (n, r, g) in enumerate(hyperparameters):
        print('Testing hyperparameter selection {}/{}'.format(index + 1, len(hyperparameters)))
        print(n, r, g)
        aucs = []
        training_aucs = []
        unweighted_aucs = []
        scaled_scores = []
        scores = []
        k_fold = StratifiedGroupKFold(n_splits=10)
        for train_index, test_index in k_fold.split(filenames, Y, groups):
            svm = SVC(C=10**r, gamma=10**g, kernel='rbf', max_iter=10**7)
            filenames_train, Y_train = filenames[train_index], Y[train_index]
            filenames_test, Y_test = filenames[test_index], Y[test_index]

            vectorizer = TfidfVectorizer(input='filename', min_df=2)
            X_train = vectorizer.fit_transform(filenames_train).toarray()
            X_test = vectorizer.transform(filenames_test).toarray()

            transformer = GenericUnivariateSelect(mode='k_best', param=n)
            X_train = transformer.fit_transform(X_train, Y_train)
            X_test = transformer.transform(X_test)
            # feature_indices = transformer.get_support(indices=False)

            svm.fit(X_train, Y_train)
            scores.append(svm.score(X_test, Y_test))
            scaled_scores.append(svm.score(X_test, Y_test) * len(test_index))
            aucs.append(len(test_index) * roc_auc_score(Y_test, svm.decision_function(X_test)))
            training_aucs.append(roc_auc_score(Y_train, svm.decision_function(X_train)))
            unweighted_aucs.append(roc_auc_score(Y_test, svm.decision_function(X_test)))

        results[(n, r, g)] = (sum(aucs) / len(Y), sum(scaled_scores) / len(Y))

    return results


if __name__ == '__main__':
    n_features = range(100, 300, 5)
    reg_exponents = np.arange(4, 20, 1)
    gamma_exponents = np.arange(-8, 2, 1)

    parameters = [(n, int(r), int(g)) for n in n_features for r in reg_exponents for g in gamma_exponents]

    results = do_classification(parameters)
    keys = list(results.keys())
    for k in sorted(keys, key=lambda k: results[k][0], reverse=True):
        print(k, results[k])
