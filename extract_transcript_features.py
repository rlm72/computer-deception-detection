"""
Extract features from transcripts using the pretrained GloVe word vectors.
"""
import pickle
import os

import pandas as pd
from nltk.tokenize import TweetTokenizer
from sklearn.feature_extraction.text import TfidfVectorizer

from utils import transcript_filename
from constants import GLOVE_ORIGINAL_LOCATION, GLOVE_DICT_LOCATION, TRANSCRIPTS_LOCATION, CLIP_COUNTS


def transcript_file_paths():
    """
    Yield the file paths associated with all transcripts, in the standard order.
    """
    for category in ('Deceptive', 'Truthful'):
        for i in range(1, CLIP_COUNTS[category] + 1):
            filename = transcript_filename(category, i)
            yield os.path.join(TRANSCRIPTS_LOCATION, category, filename)


def get_used_words():
    """
    Find words that are used in any transcript of the Real-life Deception Detection dataset.
    """
    result = set()
    tokenizer = TweetTokenizer()
    for file_path in transcript_file_paths():
        with open(file_path) as f:
            tokens = tokenizer.tokenize(f.read())

        print(tokens)
        for t in tokens:
            result.add(t.lower())

    return result


def filter_glove():
    """
    Load the GloVe vectors and write them to a dict for quicker access in future, only keeping words that actually
    appear in transcripts.
    """
    words = get_used_words()
    df = pd.read_csv(GLOVE_ORIGINAL_LOCATION, sep=" ", quoting=3, header=None, index_col=0)
    glove = {key: val.values for key, val in df.T.items() if key in words}

    with open(GLOVE_DICT_LOCATION, 'wb') as f:
        pickle.dump(glove, f)


def load_glove():
    with open(GLOVE_DICT_LOCATION, 'rb') as f:
        return pickle.load(f)


def frequency_features(filenames):
    """
    Create features representing frequency counts of words in the transcripts.

    Use TF-IDF weighting to de-emphasise common words.
    """
    vectorizer = TfidfVectorizer(input='filename', min_df=2)
    corpus = list(transcript_file_paths())

    features = vectorizer.fit_transform(corpus)
    return features, vectorizer


if __name__ == '__main__':
    fs, v = frequency_features()
    print(v.vocabulary_)
    print(fs.toarray())
