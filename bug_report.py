import numpy as np
from sklearn.feature_selection import VarianceThreshold
from scipy.sparse import csc_matrix

works_correctly = np.array([[-0.13725701,  7.        ],
                            [-0.13725701, -0.09853293],
                            [-0.13725701, -0.09853293],
                            [-0.13725701, -0.09853293],
                            [-0.13725701, -0.09853293],
                            [-0.13725701, -0.09853293],
                            [-0.13725701, -0.09853293],
                            [-0.13725701, -0.09853293],
                            [-0.13725701, -0.09853293]])

broken = np.array([[-0.13725701,  7.        ],
                   [-0.13725701, -0.09853293],
                   [-0.13725701, -0.09853293],
                   [-0.13725701, -0.09853293],
                   [-0.13725701, -0.09853293],
                   [-0.13725701, -0.09853293],
                   [-0.13725701, -0.09853293],
                   [-0.13725701, -0.09853293],
                   [-0.13725701, -0.09853293],
                   [-0.13725701, -0.09853293]])

broken_sparse = csc_matrix(broken)


selector = VarianceThreshold()
print(selector.fit_transform(works_correctly))
print(selector.variances_)

selector = VarianceThreshold()
print(selector.fit_transform(broken))
print(selector.variances_)

selector = VarianceThreshold()
print(selector.fit_transform(broken_sparse))
print(selector.variances_)
print(set(broken[:, 0]))

print(np.var(broken[:, 0]))
b=broken[:, 0]
print(np.mean(b))
mean = np.mean(b)
print((b - mean)**2)
print(sum(b) / len(b))
