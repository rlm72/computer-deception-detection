"""
Utility functions and classes for the deception detection system.
"""
import os
import csv

import numpy as np
import pandas as pd
from sklearn.model_selection import GroupKFold
from sklearn.utils.validation import check_array

from constants import CLIP_NAME_PARTS, CLIP_COUNTS, FEATURES_LOCATION


def sample_filename(category, number, extension):
    """
    Return the filename for a sample file from the Real Life Deception Detection Dataset in a given category with a given number.
    """
    return 'trial_{}_{}.{}'.format(CLIP_NAME_PARTS[category], str(number).zfill(3), extension)


def clip_filename(category, number):
    return sample_filename(category, number, 'mp4')


def transcript_filename(category, number):
    return sample_filename(category, number, 'txt')


def get_used_videos():
    """
    Return a list of videos from the Real Life Deception Detection Dataset that will be used.
    """
    with open(os.path.join('data', 'used_videos.txt')) as f:
        return set([line.strip() for line in f.readlines()])


def all_filenames_category(extension, category):
    """
    Return all filenames for used files in a category in the standard order (deceptive then truthful), with a given extension.
    """
    used_videos = get_used_videos()
    for i in range(1, CLIP_COUNTS[category] + 1):
        filename = sample_filename(category, i, '')[:-1]    # remove the dot
        if filename in used_videos:
            yield sample_filename(category, i, extension)


def all_filenames(extension):
    """
    Return all filenames for used files in the standard order (deceptive then truthful), with a given extension.
    """
    for category in ('Deceptive', 'Truthful'):
        for filename in all_filenames_category(extension, category):
            yield filename


def get_raw_features_with_filenames(category):
    """
    Return a list of (filename, dataframe) pairs for the videos in a category of the Real Life Deception Detection
    Dataset.

    The filenames should not have extensions.
    """
    used_files = get_used_videos()

    au_datasets = []
    for i in range(1, CLIP_COUNTS[category] + 1):
        clip_name = clip_filename(category, i)[:-4]        # remove .mp4
        file_name = os.path.join(FEATURES_LOCATION, clip_name + '.csv')

        if clip_name in used_files:
            au_datasets.append((clip_name, pd.read_csv(file_name)))
        else:
            print('{} is not in the list of videos to consider'.format(clip_name))

    return au_datasets


def get_raw_features(category):
    """
    Return a list of dataframes for the videos in a category (deceptive or truthful) of the Real Life Deception
    Detection Dataset.
    """
    return [dataframe for clip_name, dataframe in get_raw_features_with_filenames(category)]


def get_all_raw_features():
    """
    Return a list of dataframes for all the videos in the Real Life Deception Detection Dataset that are being used.
    """
    return get_raw_features('Deceptive') + get_raw_features('Truthful')


def get_all_raw_features_with_filenames():
    """
    Return a list of (filename, dataframe) pairs for all the videos of the Real Life Deception Detection
    Dataset.
    """
    return get_raw_features_with_filenames('Deceptive') + get_raw_features_with_filenames('Truthful')


def get_subjects():
    """
    Return a Series of subject names for the videos in the Real Life Deception Detection Dataset that are being used.
    """
    used_files = get_used_videos()
    with open(os.path.join('data', 'video_subjects.csv')) as f:
        reader = csv.reader(f, escapechar='\\')
        return pd.Series([name.strip() for video, name in reader if video in used_files])


class StratifiedGroupKFold(GroupKFold):
    """
    Cross-validator for binary classification similar to GroupKFold, except that it ensures (if possible) that all folds
    contain samples from both classes.
    """
    def _iter_test_indices(self, X, y, groups):
        # Most of this is taken from GroupKFold
        if groups is None:
            raise ValueError("The 'groups' parameter should not be None.")
        groups = check_array(groups, ensure_2d=False, dtype=None)

        unique_groups, groups = np.unique(groups, return_inverse=True)
        n_groups = len(unique_groups)
        unique_group_indices = np.arange(len(unique_groups))

        if self.n_splits > n_groups:
            raise ValueError("Cannot have number of splits n_splits=%d greater"
                             " than the number of groups: %d."
                             % (self.n_splits, n_groups))

        # Weight groups by their number of occurrences
        n_samples_per_group = np.bincount(groups)

        # This logic differs from GroupKFold
        zero_ratio = np.bincount(y)[0] / len(y)
        group_counts = np.bincount(groups)
        group_zero_ratios = np.bincount(groups[y == 0], minlength=len(unique_groups)) / group_counts
        low_ratio_groups = unique_group_indices[group_zero_ratios < zero_ratio]
        equal_ratio_groups = unique_group_indices[group_zero_ratios == zero_ratio]
        high_ratio_groups = unique_group_indices[group_zero_ratios > zero_ratio]
        ratio_groups = (low_ratio_groups, high_ratio_groups, equal_ratio_groups)
        indices = np.argsort(abs(group_zero_ratios - zero_ratio))
        n_samples_per_group = n_samples_per_group[indices]

        zeros_per_fold = np.zeros(self.n_splits)
        ones_per_fold = np.zeros(self.n_splits)

        # Absolute difference between ratio of zeros in each fold to datset ratio
        # Set to impossibly high value to avoid floating point errors
        balance_per_fold = np.ones(self.n_splits)

        # Mapping from group index to fold index
        group_to_fold = np.zeros(len(unique_groups))

        # Record of which groups have already been distributed
        used_groups = np.zeros(len(unique_groups), dtype=bool)

        # Distribute samples by adding unbalanced groups to folds that are unbalanced
        # in the opposite direction
        # for group_index, weight in enumerate(n_samples_per_group):
        while not used_groups.all():
            least_balanced_fold = np.argmax(balance_per_fold)

            # find a group that will make it balanced
            if zeros_per_fold[least_balanced_fold] == ones_per_fold[least_balanced_fold] == 0:
                # fold is currently empty
                fold_ratio = None
            else:
                fold_ratio = zeros_per_fold[least_balanced_fold] / (zeros_per_fold[least_balanced_fold] + ones_per_fold[least_balanced_fold])

            if fold_ratio is None:
                # use the largest ratio category
                ratio_index = np.argmax([len(g) for g in ratio_groups])
            elif fold_ratio > zero_ratio:
                ratio_index = 0
            elif fold_ratio < zero_ratio:
                ratio_index = 1
            else:
                ratio_index = 2

            # get the unused groups with the ratio that pushes the balance in the correct direction
            possible_groups = ratio_groups[ratio_index][~used_groups[ratio_groups[ratio_index]]]
            if len(possible_groups) == 0:
                if balance_per_fold[least_balanced_fold] in (0, 1) and fold_ratio is not None:
                    # there is no way to avoid the group having samples from only one class
                    raise ValueError('It is not possible to divide samples into stratified folds with the groups given')

                else:
                    # try groups with suboptimal ratios
                    for i in range(1, 3):
                        r_index = (ratio_index + i) % 3
                        possible_groups = ratio_groups[r_index][~used_groups[ratio_groups[r_index]]]
                        if len(possible_groups) > 0:
                            break
                    else:
                        # we have run out of groups, even though there are some remaining according to `used_groups`
                        raise ValueError('Unexpected error in creating folds for cross validation')

            chosen_group = possible_groups[0]

            used_groups[chosen_group] = True

            zeros_per_fold[least_balanced_fold] += len(groups[(groups == chosen_group) & (y == 0)])
            ones_per_fold[least_balanced_fold] += len(groups[(groups == chosen_group) & (y == 1)])
            fold_ratio = zeros_per_fold[least_balanced_fold] / (zeros_per_fold[least_balanced_fold] + ones_per_fold[least_balanced_fold])
            balance_per_fold[least_balanced_fold] = abs(fold_ratio - zero_ratio)
            group_to_fold[chosen_group] = least_balanced_fold

        indices = group_to_fold[groups]

        for f in range(self.n_splits):
            yield np.where(indices == f)[0]
