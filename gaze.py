"""
Extract features to do with gaze from raw OpenFace features.
"""
import os

import pandas as pd
import matplotlib
matplotlib.use('TkAgg')
import matplotlib.pyplot as plt    # noqa: E402

from constants import EYE_LANDMARK_FEATURES_LOCATION    # noqa: E402
from utils import all_filenames_category    # noqa: E402

def get_gaze_features(gaze_datasets):
    """
    Produce mean and standard deviation features from gaze feature sets.
    """
    df = pd.DataFrame()
    for c in (' gaze_angle_x', ' gaze_angle_y'):
        df[c.strip() + '_mean'] = pd.Series([dataset[c].mean() for dataset in gaze_datasets]).fillna(0)
        df[c.strip() + '_std'] = pd.Series([dataset[c].std() for dataset in gaze_datasets]).fillna(0)

    return df


if __name__ == '__main__':
    figure, axes = plt.subplots(4, 2, figsize=(12, 15), sharex='row', sharey='row')
    for index, category in enumerate(('Deceptive', 'Truthful')):
        x_means = []
        y_means = []
        x_stds = []
        y_stds = []

        for filename in all_filenames_category('csv', category):
            df = pd.read_csv(os.path.join(EYE_LANDMARK_FEATURES_LOCATION, filename))
            df.rename(columns=lambda x: x.strip(), inplace=True)
            x_means.append(df.gaze_angle_x.mean())
            y_means.append(df.gaze_angle_y.mean())
            x_stds.append(df.gaze_angle_x.std())
            y_stds.append(df.gaze_angle_y.std())

        x_means = pd.Series(x_means)
        y_means = pd.Series(y_means)
        x_stds = pd.Series(x_stds)
        y_stds = pd.Series(y_stds)

        print(x_means.mean(), x_means.std())
        print(y_means.mean(), y_means.std())
        print(x_stds.mean(), x_stds.std())
        print(y_stds.mean(), y_stds.std())

        print()

        axes[0, index].hist(x_means)
        axes[1, index].hist(y_means)
        axes[2, index].hist(x_stds)
        axes[3, index].hist(y_stds)

    figure.savefig('gaze_graphs.pdf', bbox_inches='tight')
