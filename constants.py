"""
Constants that are used across the deception detection system.
"""
import os

DISK_LOCATION = '/Volumes/sandisk'
OPENFACE_LOCATION = os.path.join(DISK_LOCATION, 'OpenFace/build/bin')
DATASET_LOCATION = os.path.join(DISK_LOCATION, 'Real-life_Deception_Detection_2016')
TRANSCRIPTS_LOCATION = os.path.join(DATASET_LOCATION, 'Transcription')
FEATURES_LOCATION = os.path.join(DISK_LOCATION, 'project/features')
EYE_LANDMARK_FEATURES_LOCATION = FEATURES_LOCATION + '_eye_landmarks'
GLOVE_ORIGINAL_LOCATION = os.path.join(DISK_LOCATION, 'glove.840B.300d.txt')
GLOVE_DICT_LOCATION = os.path.join(DISK_LOCATION, 'glove.pkl')

DECEPTIVE_COUNT = 61
TRUTHFUL_COUNT = 60
CLIP_COUNTS = {'Deceptive': DECEPTIVE_COUNT, 'Truthful': TRUTHFUL_COUNT}
UNUSED_COUNTS = {'Deceptive': 7, 'Truthful': 10}
CLIP_NAME_PARTS = {'Deceptive': 'lie', 'Truthful': 'truth'}

# columns in the CSVs produced by OpenFace
FEATURES = [' AU{}'.format(str(i).zfill(2)) for i in (1, 2, 4, 5, 6, 7, 9, 10, 12, 14, 15, 17, 20, 23, 25, 26, 45)]
INTENSITY_FEATURE_NAMES = [f + '_r' for f in FEATURES]
PRESENCE_FEATURE_NAMES = [f + '_c' for f in FEATURES]
USED_FEATURE_NAMES = INTENSITY_FEATURE_NAMES + [' AU28_c']
