def do_ftv_classification(hyperparameters):
    """
    Perform a round of cross-validation using FTVs as features.
    """
    results = {}

    for index, (n, r, g) in enumerate(hyperparameters):
        print('Testing hyperparameter selection {}/{}'.format(index + 1, len(hyperparameters)))
        print(n, r, g)
        aucs = []
        training_aucs = []
        unweighted_aucs = []
        scaled_scores = []
        scores = []
        k_fold = StratifiedGroupKFold(n_splits=10)
        for train_index, test_index in k_fold.split(X, Y, groups):
            feature_names = df.columns[:-1]    # omitting class
            X_train, Y_train = X[train_index], Y[train_index]
            datasets_train, filenames_train = au_datasets[train_index], filenames[train_index]
            X_test, Y_test = X[test_index], Y[test_index]
            datasets_test, filenames_test = au_datasets[test_index], filenames[test_index]

            assert len(Y_train) == len(datasets_train) == len(filenames_train)
            assert len(Y_test) == len(datasets_test) == len(filenames_test)
            ftv_train = get_FTV_dataset(zip(filenames_train, datasets_train), Y_train, 30)
            ftv_test = get_FTV_dataset(zip(filenames_test, datasets_test), Y_test, 30)

            roc, ftv_pred_classes = classify_ftvs(ftv_train, ftv_test)
            print(roc)
            print(len(ftv_test), len(ftv_pred_classes))

            ver_classes = []
            ftv_scores = []
            for filename in filenames_test:
                count = ftv_test['filename'].value_counts()[filename]
                ver_classes.append(ftv_test[ftv_test['filename'] == filename]['class'].sum() / count)
                ftv_scores.append(ftv_pred_classes[ftv_test['filename'] == filename].sum() / count)

            ex = pd.DataFrame()
            ex['y'] = Y_test
            ex['ver'] = pd.Series(ver_classes, index=ex.index)
            ex['ftv'] = pd.Series(ftv_scores, index=ex.index)
            ex['fns'] = filenames_test

            print(ex)
            continue

            svm = SVC(C=10**r, gamma=10**g, kernel='rbf', max_iter=10**7)

            selector = VarianceThreshold(1e-25)
            unscaled_train = unscaled_X[train_index]
            unscaled_train = selector.fit_transform(unscaled_train)

            X_train = selector.transform(X_train)
            X_test = selector.transform(X_test)

            feature_names = feature_names[selector.get_support(indices=False)]

            transformer = GenericUnivariateSelect(mode='k_best', param=n)
            X_train = transformer.fit_transform(X_train, Y_train)
            X_test = transformer.transform(X_test)
            # feature_indices = transformer.get_support(indices=False)

            svm.fit(X_train, Y_train)
            scores.append(svm.score(X_test, Y_test))
            scaled_scores.append(svm.score(X_test, Y_test) * len(test_index))
            aucs.append(len(test_index) * roc_auc_score(Y_test, svm.decision_function(X_test)))
            training_aucs.append(roc_auc_score(Y_train, svm.decision_function(X_train)))
            unweighted_aucs.append(roc_auc_score(Y_test, svm.decision_function(X_test)))

        results[(n, r, g)] = (sum(aucs) / len(Y), sum(scaled_scores) / len(Y))
        input()

    return results
