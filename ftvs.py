"""
Generate FTV (Feature Temporal Volume) features, as per (Lin Su, 2013).
"""
import os

import numpy as np
import pandas as pd
import tensorflow as tf
from tensorflow import keras
from sklearn import preprocessing
from sklearn.feature_selection import VarianceThreshold
from sklearn.model_selection import StratifiedKFold
from sklearn.metrics import roc_auc_score

from utils import get_all_raw_features_with_filenames
from constants import CLIP_COUNTS, UNUSED_COUNTS, PRESENCE_FEATURE_NAMES

FTV_FILES_LOCATION = os.path.join('data', 'ftvs')


def get_FTVs(dataset, context_length, filename=None):
    """
    Given a dataset, return a dataframe of FTVs with a given context length.

    If `filename` is present, attempt to lookup the result in that location first.
    """
    if filename is not None:
        file_path = os.path.join(FTV_FILES_LOCATION, '{}_{}.csv'.format(filename, context_length))

        try:
            result = pd.read_csv(file_path)
            return result
        except FileNotFoundError:
            pass        # calculate and write to file

    result = []
    df = dataset[PRESENCE_FEATURE_NAMES + [' AU28_c']]
    for i in range(dataset.shape[0] - context_length + 1):
        result.append(df.iloc[i:i + context_length].sum())

    result = pd.DataFrame(result)

    if filename is not None:
        os.makedirs(os.path.dirname(file_path), exist_ok=True)
        result.to_csv(file_path, index=False)

    return result


def get_FTV_dataset(filenames_and_au_datasets, classes, context_length):
    """
    Given a list of (filename, datasets) tuples and associated class labels, return a dataframe containing the bag of
    FTVs for each video in the dataset and associated classes.
    """
    filenames_and_au_datasets, classes = list(filenames_and_au_datasets), list(classes)
    ftv_frames = []
    if len(filenames_and_au_datasets) != len(classes):
        raise ValueError('Number of datasets and classes must be equal')

    for (filename, dataset), cls in zip(filenames_and_au_datasets, classes):
        ftvs = get_FTVs(dataset, context_length, filename=filename)
        ftvs['class'] = pd.Series([cls] * ftvs.shape[0])
        ftvs['filename'] = pd.Series([filename] * ftvs.shape[0])
        ftv_frames.append(ftvs)

    return pd.concat(ftv_frames)


def classify_ftvs(ftv_train, ftv_test):
    """
    Given training and test set FTV dataframes, train a classifier on the training set and use it on the test set.
    """
    X = pd.concat([ftv_train, ftv_test]).drop('class', axis=1).drop('filename', axis=1)
    X = X.drop(' AU12_c', axis=1)
    X = X.drop(' AU14_c', axis=1)

    # print(X.columns, len(X.columns), len(PRESENCE_FEATURE_NAMES) + 1)
    # print(X, X.shape)
    X = X.astype(float)
    X = preprocessing.scale(X)
    # print(X, X.shape)
    # input()

    Y_train = ftv_train['class']
    Y_test = ftv_test['class']

    X_train = X[:ftv_train.shape[0]]
    X_test = X[ftv_train.shape[0]:]
    assert X_train.shape[0] == ftv_train.shape[0] == Y_train.shape[0]
    assert X_test.shape[0] == ftv_test.shape[0] == Y_test.shape[0]

    # clf = LogisticRegression(max_iter=3000, C=1000)
    # clf = RandomForestClassifier()
    # clf = LinearSVC()
    # clf = BaggingClassifier(base_estimator=SVC(kernel='rbf'), max_samples=0.1, n_jobs=-1, bootstrap=False)
    clf = keras.Sequential()
    clf.add(keras.layers.Dense(20, activation=tf.nn.relu,
                               kernel_regularizer=keras.regularizers.l2(0.03)))

    clf.add(keras.layers.Dense(1, activation=tf.nn.sigmoid,
                               kernel_regularizer=keras.regularizers.l2(0.03)))

    clf.compile(optimizer=keras.optimizers.Adam(),
                loss='binary_crossentropy')

    selector = VarianceThreshold()
    X_train = selector.fit_transform(X_train)
    X_test = selector.transform(X_test)

    clf.fit(X_train, np.array(Y_train), verbose=0, epochs=500)
    roc = roc_auc_score(Y_test, clf.predict_proba(X_test)[:, 1])
    # roc = roc_auc_score(Y_test, clf.decision_function(X_test))
    print('Acc', clf.score(X_test, np.array(Y_test)))
    return roc, clf.predict(X_test)


if __name__ == '__main__':
    filenames_and_au_datasets = get_all_raw_features_with_filenames()
    classes = [1] * (CLIP_COUNTS['Deceptive'] - UNUSED_COUNTS['Deceptive']) + [0] * (CLIP_COUNTS['Truthful'] - UNUSED_COUNTS['Truthful'])

    for t in range(30, 70, 1):
        print('Getting for', t)
        ftvs = get_FTV_dataset(filenames_and_au_datasets, classes, t)
        k_fold = StratifiedKFold(n_splits=10, shuffle=True)  # True, random_state=17)
        X = ftvs.drop('class', axis=1).drop('filename', axis=1)
        Y = ftvs['class']

        rocs = []
        for train_index, test_index in k_fold.split(X, Y):
            roc, pred = classify_ftvs(ftvs.iloc[train_index], ftvs.iloc[test_index])
            rocs.append(roc)

        print(pd.Series(rocs).mean())
