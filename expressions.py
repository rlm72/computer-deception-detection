"""
Extract expressions from raw OpenFace features.

Expressions are repeated occurences of AUs.
"""
import os
import json
from multiprocessing import Pool
from functools import wraps
from collections import defaultdict, Counter, namedtuple
from time import time

import pandas as pd
import numpy as np
import matplotlib
matplotlib.use('TkAgg')
import matplotlib.pyplot as plt    # noqa: E402
from sklearn.feature_selection import VarianceThreshold, GenericUnivariateSelect    # noqa: E402

from constants import FEATURES, CLIP_COUNTS    # noqa: E402
from utils import get_raw_features_with_filenames, get_all_raw_features_with_filenames    # noqa: E402
from interval_trees import Interval, all_intersections    # noqa: E402

EXPRESSION_FILES_LOCATION = os.path.join('data', 'expressions')


def expression_function_cache(filename_suffix):
    """
    Cache the results of a function as a JSON file.
    """
    def expression_function_cache_decorator(func):
        @wraps(func)
        def wrapper(*args, **kwargs):
            cached = kwargs.get('cached', False)
            filename = kwargs.get('filename')
            if cached and not filename:
                raise ValueError('A filename must be supplied if using the cache')

            if cached:
                file_path = os.path.join(EXPRESSION_FILES_LOCATION, filename + '_' + filename_suffix + '.json')

                try:
                    with open(file_path) as f:
                        result = json.load(f)
                        return result
                except FileNotFoundError:
                    pass        # calculate it and write to cache

            result = func(*args, **kwargs)

            if cached:
                os.makedirs(os.path.dirname(file_path), exist_ok=True)
                with open(file_path, 'w') as f:
                    json.dump(result, f)

            return result
        return wrapper
    return expression_function_cache_decorator


def get_inside_expression_predicate(use_presence, intensity_threshold):
    """
    Return a predicate for use with `get_expressions` for a given intensity threshold.
    """
    if use_presence:
        predicate = lambda presence, intensity: presence == 1 and intensity > intensity_threshold    # noqa: E731
    else:
        predicate = lambda presence, intensity: intensity > intensity_threshold    # noqa: E731

    return predicate


@expression_function_cache('expressions')
def get_expressions(dataset, inside_expression_predicate=get_inside_expression_predicate(False, 1.25), cached=False, filename=None,
                    use_AU28=True):
    """
    Extract the expressions from the AU dataset for one video.

    Returns a dictionary mapping feature names to `(start_frame, end_frame)` pairs for parts of the video where that feature was
    present.

    A frame is considered to be inside an expression for an AU if `inside_expression_predicate` returns `True` when
    applied to the presence and intensity of the AU at that frame.

    `start_frame` is inclusive, `end_frame` is exclusive.

    If `cached`, try to use results in '`EXPRESSION_FILES_LOCATION`/`filename`_expressions.json' rather than calculating.
    """
    expressions = {}
    if use_AU28:
        features = FEATURES + [' AU28']
    else:
        features = FEATURES

    for f in features:
        expressions[f] = []
        inside_expression = False
        start = end = 0
        presences = dataset[f + '_c'].to_numpy()
        if f != ' AU28':
            intensities = dataset[f + '_r'].to_numpy()

        for index in range(len(dataset)):
            if f == ' AU28':
                presence = presences[index]
                predicate_value = (presence == 1)
            else:
                presence, intensity = presences[index], intensities[index]
                predicate_value = inside_expression_predicate(presence, intensity)

            if inside_expression and predicate_value:
                end = index + 1

            elif inside_expression and not predicate_value:
                expressions[f].append((start, end))
                inside_expression = False

            elif not inside_expression and predicate_value:
                start = index
                end = index + 1
                inside_expression = True

            elif not inside_expression and not predicate_value:
                pass

    return expressions


def lengths(expressions):
    """
    Return the lengths of expressions for each feature in the dictionary returned by `get_expressions`.
    """
    return {f: [end - start for (start, end) in expressions[f]] for f in expressions}


def intensities(dataset, expressions):
    """
    Return the average intensities of expressions for each feature in the dictionary returned by `get_expressions`.
    """
    result = {}
    for f in FEATURES:
        means = []
        indices = expressions[f]
        for (start, end) in indices:
            means.append(dataset[start:end][f + '_r'].mean())
        result[f] = means

    return result


def get_all_overlaps(dataset, intensity_threshold=1.25, use_presence=False):
    """
    Return all indices where expressions occur simultaneously in different AUs in a video
    using interval trees.
    """
    expressions = get_expressions(dataset, cached=False)
    interval_lists = [[Interval(f, start, end) for start, end in expressions[f]]
                      for f in expressions]

    return all_intersections(interval_lists)


def overlap_length(overlap):
    """
    Helper function to get the length of an overlap, expressed as a pair of intervals.
    """
    (a, b), (c, d) = overlap
    return min(b, d) - max(a, c)


def get_overlaps(dataset, minimum_proportion_overlapping=0, minimum_length=0, maximum_length=10**6,
                 cached=False, filename=None, intensity_threshold=1.25, use_presence=False):
    """
    Return indices where expressions occur simultaneously in different AUs in a video.

    `minimum_proportion_overlapping` is the minimum ratio of overlap length to total length covered by the intervals
    to be counted.

    `minimum_length` and `maximum_length` restrict the length overlaps return can have and are both inclusive.

    If `cached`, attempt to use precalculated results stored in '`filename`_overlaps.json'.
    """
    result = {}
    all_overlaps = get_all_overlaps(dataset, intensity_threshold=intensity_threshold, use_presence=use_presence)
    for key in all_overlaps:
        result[key] = [((a, b), (c, d)) for ((a, b), (c, d)) in all_overlaps[key]
                       if overlap_length(((a, b), (c, d))) / (max(b, d) - min(a, c)) >= minimum_proportion_overlapping
                       and minimum_length <= overlap_length(((a, b), (c, d))) <= maximum_length]
    return result


def _get_overlap_counts(dataset, filename, intensity_threshold=0.2, length_threshold=70,
                        use_presence=False, minimum_proportion_overlapping=0.5):
    """
    Helper function for `get_overlap_features`.
    """

    short_overlaps = get_overlaps(dataset, minimum_proportion_overlapping=minimum_proportion_overlapping,
                                  cached=False, filename=filename, maximum_length=length_threshold,
                                  intensity_threshold=intensity_threshold, use_presence=False)
    long_overlaps = get_overlaps(dataset, minimum_proportion_overlapping=minimum_proportion_overlapping,
                                 cached=False, filename=filename, minimum_length=length_threshold,
                                 intensity_threshold=intensity_threshold, use_presence=False)
    return {k: len(short_overlaps[k]) / len(dataset) for k in short_overlaps}, \
           {k: len(long_overlaps[k]) / len(dataset) for k in long_overlaps}


def get_overlap_features(filenames_and_au_datasets, intensity_threshold=1.25, length_threshold=20, use_presence=False,
                         minimum_proportion_overlapping=0.5, hyperparameters_in_filename=True):
    async_results = [None] * len(filenames_and_au_datasets)
    print('Generating overlap features')
    with Pool(processes=8) as pool:
        for index, (filename, dataset) in enumerate(filenames_and_au_datasets):
            if hyperparameters_in_filename:
                filename = '{}_int{}_len{}'.format(filename, intensity_threshold, length_threshold)
            async_results[index] = pool.apply_async(_get_overlap_counts, [dataset, filename],
                                                    dict(intensity_threshold=intensity_threshold,
                                                         length_threshold=length_threshold,
                                                         use_presence=False))

        for index, result in enumerate(async_results):
            result.wait()

    overlap_counts = [result.get() for result in async_results]

    result = {}

    features = FEATURES + [' AU28']
    for index, f1 in enumerate(features):
        for f2 in features[index + 1:]:
            result['{}{}_short_overlap'.format(f1, f2)] = pd.Series([overlap[0].get('{}_{}'.format(f1, f2), 0)
                                                                     for overlap in overlap_counts])
            result['{}{}_long_overlap'.format(f1, f2)] = pd.Series([overlap[1].get('{}_{}'.format(f1, f2), 0)
                                                                    for overlap in overlap_counts])

    return result


def _get_expression_counts(dataset, filename, intensity_threshold=1.25, length_threshold=20, use_presence=False):
    """
    Helper function for `get_expression_features`.
    """
    predicate = get_inside_expression_predicate(use_presence, intensity_threshold)
    expressions = get_expressions(dataset, cached=False, filename=filename,
                                  inside_expression_predicate=predicate)
    short_expressions = {f: len([v for v in expressions[f] if v[1] - v[0] <= length_threshold]) / len(dataset)
                         for f in expressions}
    long_expressions = {f: len([v for v in expressions[f] if v[1] - v[0] > length_threshold]) / len(dataset)
                        for f in expressions}
    return short_expressions, long_expressions


def get_expression_features(filenames_and_au_datasets, intensity_threshold=1.25, length_threshold=20, use_presence=False, hyperparameters_in_filename=True, verbose=False):
    async_results = [None] * len(filenames_and_au_datasets)
    print('Generating expression features')
    with Pool(processes=8) as pool:
        for index, (filename, dataset) in enumerate(filenames_and_au_datasets):
            if hyperparameters_in_filename:
                filename = '{}_int{}_len{}'.format(filename, intensity_threshold, length_threshold)

            async_results[index] = pool.apply_async(_get_expression_counts, [dataset, filename],
                                                    dict(intensity_threshold=intensity_threshold,
                                                         length_threshold=length_threshold,
                                                         use_presence=use_presence))

        for index, result in enumerate(async_results):
            result.wait()

    expression_counts = [result.get() for result in async_results]

    result = {}

    features = FEATURES + [' AU28']
    for index, f in enumerate(features):
        result['{}_short_expression'.format(f)] = pd.Series([expression[0].get(f, 0) for expression in expression_counts])
        result['{}_long_expression'.format(f)] = pd.Series([expression[1].get(f, 0) for expression in expression_counts])

    if verbose:
        for k in result:
            print(k, result[k])

    return result


def graph_overlaps():
    """
    Graph distributions of lengths of overlaps of expressions in each class.

    Output is written to 'overlap_graphs.pdf'.
    """
    true_videos = get_raw_features_with_filenames('Truthful')
    true_counters = defaultdict(Counter)
    for index, (filename, dataset) in enumerate(true_videos):
        overlaps = get_overlaps(dataset, cached=True, filename=filename, minimum_proportion_overlapping=0.5)
        for k in overlaps:
            true_counters[k].update(map(overlap_length, overlaps[k]))

    false_videos = get_raw_features_with_filenames('Deceptive')
    false_counters = defaultdict(Counter)
    for index, (filename, dataset) in enumerate(false_videos):
        overlaps = get_overlaps(dataset, cached=True, filename=filename, minimum_proportion_overlapping=0.5)
        for k in overlaps:
            false_counters[k].update(map(overlap_length, overlaps[k]))

    print('Drawing overlap graphs')
    figure, axes = plt.subplots(max(len(true_counters), len(false_counters)), 2, figsize=(12, 480), sharex='row', sharey='row')
    for index, k in enumerate(sorted(list(true_counters.keys() | false_counters.keys()))):
        if k in true_counters:
            xs = np.array(list(true_counters[k].keys()))
            xs.sort()
            ys = np.array([true_counters[k][x] for x in xs])
            axes[index, 0].hist(xs, bins=[0, 5, 10, 30, 100], weights=ys)
            axes[index, 0].set_title(k)

        if k in false_counters:
            xs = np.array(list(false_counters[k].keys()))
            xs.sort()
            ys = np.array([false_counters[k][x] for x in xs])
            axes[index, 1].hist(xs, bins=[0, 5, 10, 30, 100], weights=ys)
            axes[index, 1].set_title(k)

    figure.savefig('overlap_graphs.pdf', bbox_inches='tight')


def graph_expressions():
    """
    Graph distributions of lengths of expressions in each class.

    Output is written to 'expression_graphs.pdf'.
    """
    true_videos = get_raw_features_with_filenames('Truthful')
    true_counters = defaultdict(Counter)
    for index, (filename, dataset) in enumerate(true_videos):
        expressions = get_expressions(dataset, cached=True, filename=filename)
        for k in expressions:
            true_counters[k].update(map(lambda interval: interval[1] - interval[0], expressions[k]))

    false_videos = get_raw_features_with_filenames('Deceptive')
    false_counters = defaultdict(Counter)
    for index, (filename, dataset) in enumerate(false_videos):
        expressions = get_expressions(dataset, cached=True, filename=filename)
        for k in expressions:
            false_counters[k].update(map(lambda interval: interval[1] - interval[0], expressions[k]))

    print('Drawing expression graphs')
    figure, axes = plt.subplots(max(len(true_counters), len(false_counters)), 2, figsize=(12, 60), sharex='row', sharey='row')
    for index, k in enumerate(sorted(list(true_counters.keys() | false_counters.keys()))):
        if k in true_counters:
            xs = np.array([x for x in true_counters[k].keys() if x < 100])
            xs.sort()
            ys = np.array([true_counters[k][x] for x in xs])
            axes[index, 0].bar(xs, ys, width=1, align='edge')
            axes[index, 0].set_title(k)

        if k in false_counters:
            xs = np.array([x for x in false_counters[k].keys() if x < 100])
            xs.sort()
            ys = np.array([false_counters[k][x] for x in xs])
            axes[index, 1].bar(xs, ys, width=1, align='edge')
            axes[index, 1].set_title(k)

    figure.savefig('expression_graphs.pdf', bbox_inches='tight')


def get_feature_correlations(filenames_and_au_datasets, feature_sets, feature_number):
    """
    Sort a family of features sets by how informative they are.
    """
    result = {}

    for spec, feature_set in feature_sets.items():
        df = pd.DataFrame()
        for f in feature_set:
            df[f] = feature_set[f]

        df['class'] = pd.Series([1] * CLIP_COUNTS['Deceptive'] + [0] * CLIP_COUNTS['Truthful'])

        X = df.drop('class', axis=1)
        Y = df['class']

        selector = VarianceThreshold()
        X = selector.fit_transform(X)

        transformer = GenericUnivariateSelect(mode='k_best', param=feature_number)
        X = transformer.fit(X, Y)

        p_values = transformer.pvalues_[transformer.get_support()]
        result[spec] = (sum(p_values), sorted(list(p_values)))

    return result


def generate_expressions_with_hyperparameters(filenames_and_au_datasets, intensity_thresholds, length_thresholds, use_presences, verbose=False):
    """
    Generate expression feature sets for specified hyperparameters.
    """
    result = {}
    ParamSpec = namedtuple('ParamSpec', ('intensity_threshold', 'length_threshold', 'use_presence'))
    total = len(intensity_thresholds) * len(length_thresholds) * len(use_presences)
    i = 0

    for intensity_threshold in intensity_thresholds:
        for length_threshold in length_thresholds:
            for use_presence in use_presences:
                if verbose:
                    i += 1
                    print('Generating expression with hyperparameter spec {}/{}'.format(i, total))

                feature_set = get_expression_features(filenames_and_au_datasets,
                                                      intensity_threshold=intensity_threshold,
                                                      length_threshold=length_threshold,
                                                      use_presence=use_presence,
                                                      hyperparameters_in_filename=True)

                result[ParamSpec(intensity_threshold, length_threshold, use_presence)] = feature_set

    return result


def optimise_expression_hyperparameters(intensity_thresholds, length_thresholds, use_presences):
    """
    Find which hyperparameters for expression extraction (e.g. intensity threshold) produce the most informative
    features.
    """

    filenames_and_au_datasets = get_all_raw_features_with_filenames()
    feature_sets = generate_expressions_with_hyperparameters(filenames_and_au_datasets, intensity_thresholds, length_thresholds, use_presences, verbose=True)

    return get_feature_correlations(filenames_and_au_datasets, feature_sets, 5)


def generate_overlaps_with_hyperparameters(filenames_and_au_datasets, intensity_thresholds, length_thresholds, use_presences, minimum_overlap_proportions, verbose=False):
    """
    Generate expression feature sets for specified hyperparameters.
    """
    result = {}
    OverlapParamSpec = namedtuple('OverlapParamSpec', ('intensity_threshold', 'length_threshold', 'use_presence', 'minimum_overlap_proportion'))
    total = len(intensity_thresholds) * len(length_thresholds) * len(use_presences) * len(minimum_overlap_proportions)
    i = 0

    for intensity_threshold in intensity_thresholds:
        for length_threshold in length_thresholds:
            for use_presence in use_presences:
                for minimum_overlap_proportion in minimum_overlap_proportions:
                    if verbose:
                        i += 1
                        print('Generating expression with hyperparameter spec {}/{}'.format(i, total))

                    feature_set = get_overlap_features(filenames_and_au_datasets,
                                                       intensity_threshold=intensity_threshold,
                                                       length_threshold=length_threshold,
                                                       use_presence=use_presence,
                                                       minimum_proportion_overlapping=minimum_overlap_proportion,
                                                       hyperparameters_in_filename=True)

                    result[OverlapParamSpec(intensity_threshold, length_threshold, use_presence, minimum_overlap_proportion)] = feature_set

    return result


def optimise_overlap_hyperparameters(intensity_thresholds, length_thresholds, use_presences, minimum_overlap_proportions):
    """
    Find which hyperparameters for overlap extraction (e.g. overlap threshold) produce the most informative
    features.
    """

    filenames_and_au_datasets = get_all_raw_features_with_filenames()
    feature_sets = generate_overlaps_with_hyperparameters(filenames_and_au_datasets, intensity_thresholds, length_thresholds, use_presences, minimum_overlap_proportions, verbose=True)

    return get_feature_correlations(filenames_and_au_datasets, feature_sets, 5)


def test_hyperparameter_optimisation():
    intensity_thresholds = [0.01, 0.05, 0.075, 0.1, 0.5, 0.2, 0.3, 0.5, 0.8, 1, 1.25, 1.5, 1.75, 2]
    length_thresholds = [10, 15, 20, 40, 70, 100]
    minimum_overlap_proportions = [0.05, 0.25, 0.5, 0.6, 0.8, 0.9]
    use_presences = [False]

    result = optimise_overlap_hyperparameters(intensity_thresholds, length_thresholds, use_presences, minimum_overlap_proportions)
    keys = list(result.keys())
    keys.sort(key=lambda k: result[k][0])

    for k in keys:
        print(k, result[k][0])


def time_overlap_finding():
    filenames_and_au_datasets = get_all_raw_features_with_filenames()
    n = len(filenames_and_au_datasets)

    t = time()
    for index, (_, dataset) in enumerate(filenames_and_au_datasets):
        if index % 10 == 0:
            print(100 * index / n)
        get_all_overlaps(dataset)

    print(time() - t)


if __name__ == '__main__':
    fds = get_all_raw_features_with_filenames()
    get_overlap_features(fds)
