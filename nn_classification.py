"""
Classifies videos using a neural network.
"""
import math
import random

import pandas as pd
import tensorflow as tf
from tensorflow import keras
from sklearn import preprocessing
from sklearn.feature_selection import VarianceThreshold, GenericUnivariateSelect
from sklearn.metrics import roc_auc_score

from constants import CLIP_COUNTS, UNUSED_COUNTS, INTENSITY_FEATURE_NAMES
from utils import get_all_raw_features_with_filenames, get_subjects, StratifiedGroupKFold
from expressions import get_overlap_features, get_expression_features

filenames_and_au_datasets = get_all_raw_features_with_filenames()
au_datasets = list(zip(*filenames_and_au_datasets))[1]

df = pd.DataFrame()

feature_values = []
for c in INTENSITY_FEATURE_NAMES:
    # df[c.strip().strip('_r')] = pd.Series([dataset[dataset[c] >= 3][c].mean() for dataset in au_datasets]).fillna(0)
    df[c.strip().strip('_r') + '_mean'] = pd.Series([dataset[c].mean() for dataset in au_datasets]).fillna(0)
    # df[c.strip().strip('_r') + '_nz_mean'] = pd.Series([dataset[dataset[c] > 0][c].mean() for dataset in au_datasets]).fillna(0)
    # df[c.strip().strip('_r') + '_max'] = pd.Series([dataset[c].max() for dataset in au_datasets]).fillna(0)

df['AU28'] = pd.Series([int((dataset[' AU28_c'] > 0).any()) for dataset in au_datasets])

overlap_features = get_overlap_features(filenames_and_au_datasets)
expression_features = get_expression_features(filenames_and_au_datasets)

for f in overlap_features:
    df[f] = overlap_features[f]

for f in expression_features:
    df[f] = expression_features[f]

df['class'] = pd.Series([1] * (CLIP_COUNTS['Deceptive'] - UNUSED_COUNTS['Deceptive']) + [0] * (CLIP_COUNTS['Truthful'] - UNUSED_COUNTS['Truthful']))
groups = get_subjects()
print(len(df))

X = df.drop('class', axis=1)
Y = df['class']

print(X.shape)
X = X.astype(float)
unscaled_X = X.values
X = preprocessing.scale(X)
# print(df)
# print(df.corr().unstack().sort_values().drop_duplicates())
corrs = df.corr()
cols = [c for c in list(df.columns[:-1]) if not math.isnan(corrs.loc[c, 'class'])]
cols.sort(key=lambda c: abs(corrs.loc[c, 'class']))
for c in cols:
    print(c)     # , df[c].unique())
    print(corrs.loc[c, 'class'])

n_features = range(25, 50, 1)
dropout_proportion = [0.05, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6]
nodes = [(256, 256, 256), (256, 128, 64),
        (128, 128, 128), (128, 128, 64), (128, 64, 32),        # noqa: E126
        (128, 32, 16), (64, 64, 64), (64, 32, 32), (64, 32, 16),
        (32, 32, 32), (32, 16, 16), (32, 16, 8),
        (16, 16, 16), (16, 8, 4),
        ]  # noqa: E123

table = {}
i = 0
for _ in range(500):
    n = random.choice(n_features)
    dp = random.choice(dropout_proportion)
    n1, n2, n3 = random.choice(nodes)
    i += 1
    print(100 * i / 500)
    print((n, dp, (n1, n2, n3)))
    aucs = []
    training_aucs = []
    unweighted_aucs = []
    scores = []
    k_fold = StratifiedGroupKFold(n_splits=10)
    for train_index, test_index in k_fold.split(X, Y, groups):
        print(test_index)
        feature_names = df.columns[:-1]    # omitting class

        # create classifier
        model = keras.Sequential()
        model.add(keras.layers.Dense(n1, activation=tf.nn.relu))
        model.add(keras.layers.Dropout(dp))
        model.add(keras.layers.Dense(n2, activation=tf.nn.relu))
        model.add(keras.layers.Dropout(dp))
        model.add(keras.layers.Dense(n3, activation=tf.nn.relu))
        model.add(keras.layers.Dropout(dp))
        model.add(keras.layers.Dense(1, activation=tf.nn.sigmoid))

        model.compile(optimizer=keras.optimizers.Adam(),
                      loss='binary_crossentropy')

        X_train, Y_train = X[train_index], Y[train_index]
        X_test, Y_test = X[test_index], Y[test_index]

        selector = VarianceThreshold()
        unscaled_train = unscaled_X[train_index]
        unscaled_train = selector.fit_transform(unscaled_train)
        X_train = selector.transform(X_train)
        X_test = selector.transform(X_test)
        feature_names = feature_names[selector.get_support(indices=False)]

        transformer = GenericUnivariateSelect(mode='k_best', param=n)
        X_train = transformer.fit_transform(X_train, Y_train)
        X_test = transformer.transform(X_test)
        feature_indices = transformer.get_support(indices=False)
        # print(feature_names[feature_indices])

        model.fit(X_train, Y_train.values, verbose=0, epochs=500)
        aucs.append(len(test_index) * roc_auc_score(Y_test, model.predict_proba(X_test)))
        training_aucs.append(roc_auc_score(Y_train, model.predict_proba(X_train)))
        unweighted_aucs.append(roc_auc_score(Y_test, model.predict_proba(X_test)))
        # _ = input()

    print(len(aucs))
    print('Weighted AUCs', aucs)
    print('training AUCs', training_aucs)
    print('Unweighted AUCs', unweighted_aucs)
    print('Average AUC', sum(aucs) / len(Y))
    table[(n, dp, (n1, n2, n3))] = sum(aucs) / len(Y)
    print()

    keys = sorted(list(table.keys()), key=lambda k: table[k])
    for k in keys:
        print(k, table[k])
