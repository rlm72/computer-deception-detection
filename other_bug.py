import numpy as np
from sklearn.neighbors import KernelDensity
data = np.reshape([1., 2., 3., 1., 2., 3.], (-1, 2))
print(data)
kde = KernelDensity(bandwidth=0.3)
kde.fit(data, sample_weight=[0.1,0.2,0.3])
from sklearn.externals import joblib
joblib.dump(kde, './test_kde.pkl')
