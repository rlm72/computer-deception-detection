"""
Classifies videos based on previously extracted features.
"""
import os
import logging.config
import warnings
import gc
from collections import namedtuple
from itertools import product

import numpy as np
import pandas as pd
from sklearn import preprocessing
from sklearn.feature_selection import VarianceThreshold, GenericUnivariateSelect
from sklearn.svm import SVC
from sklearn.metrics import roc_auc_score
from sklearn.exceptions import ConvergenceWarning

from constants import CLIP_COUNTS, UNUSED_COUNTS, INTENSITY_FEATURE_NAMES, EYE_LANDMARK_FEATURES_LOCATION
from utils import get_all_raw_features_with_filenames, get_subjects, StratifiedGroupKFold, all_filenames
from expressions import get_overlap_features, get_expression_features
from gaze import get_gaze_features

logging.config.fileConfig('logging.conf')

logger = logging.getLogger()
output_logger = logging.getLogger('evaluation')

Hyperparameters = namedtuple('Hyperparameters', 'clf n')


def load_features(intensities=True, expressions=True, overlaps=True, gaze=True):
    """
    Load and create the features for classification.
    """
    if not any((intensities, expressions, overlaps, gaze)):
        raise ValueError('No features selected for classification')

    df = pd.DataFrame()

    filenames_and_au_datasets = get_all_raw_features_with_filenames()
    filenames, au_datasets = map(pd.Series, zip(*filenames_and_au_datasets))

    n_samples = len(au_datasets)

    if intensities:
        for c in INTENSITY_FEATURE_NAMES:
            df[c.strip().strip('_r') + '_mean'] = pd.Series([dataset[c].mean() for dataset in au_datasets]).fillna(0)

        df['AU28'] = pd.Series([int((dataset[' AU28_c'] > 0).any()) for dataset in au_datasets])

        assert len(df) == n_samples

    expression_features = overlap_features = gaze_features = None

    if expressions:
        expression_features = get_expression_features(filenames_and_au_datasets)

    if overlaps:
        overlap_features = get_overlap_features(filenames_and_au_datasets)

    if gaze:
        gaze_datasets = [pd.read_csv(os.path.join(EYE_LANDMARK_FEATURES_LOCATION, f)) for f in all_filenames('csv')]
        gaze_features = get_gaze_features(gaze_datasets)

    for include, feature_set in ((expressions, expression_features),
                                 (overlaps, overlap_features),
                                 (gaze, gaze_features)):
        if include:
            for f in feature_set:
                try:
                    assert len(feature_set[f]) == n_samples
                except AssertionError:
                    logger.warning(len(feature_set[f]), n_samples)
                    logger.info(f)
                    logger.info(feature_set[f])
                df[f] = feature_set[f]

    classes = pd.Series([1] * (CLIP_COUNTS['Deceptive'] - UNUSED_COUNTS['Deceptive']) + [0] * (CLIP_COUNTS['Truthful'] - UNUSED_COUNTS['Truthful']))
    assert len(classes) == n_samples
    df['class'] = classes

    X = df.drop('class', axis=1)
    Y = df['class']
    groups = get_subjects()

    feature_names = df.columns[:-1]    # omitting class
    assert len(groups) == n_samples

    return X, Y, groups, feature_names



def classification_step(clf, hyperparameters, X, Y, train_index, test_index):
    """
    Do one iteration of feature selection, training, and prediction.
    """
    X = X.values

    n = hyperparameters.n

    X_train, Y_train = X[train_index], Y[train_index]
    X_test, Y_test = X[test_index], Y[test_index]

    selector = VarianceThreshold(1e-30)

    X_train = selector.fit_transform(X_train)
    X_test = selector.transform(X_test)

    transformer = GenericUnivariateSelect(mode='k_best', param=n)
    X_train = transformer.fit_transform(X_train, Y_train)
    X_test = transformer.transform(X_test)

    clf.fit(X_train, Y_train)

    if hasattr(clf, 'decision_function'):
        prediction_score = clf.decision_function(X_test)
    else:
        prediction_score = clf.predict_proba(X_test)

    auc = roc_auc_score(Y_test, prediction_score)
    accuracy = clf.score(X_test, Y_test)
    return auc, accuracy


def hyperparameter_search(hyperparameter_space, X, Y, groups, classifier, skip_broken_hyperparams=True):
    """
    Return the performance for each set of hyperparameters
    """
    Y = Y.reset_index(drop=True)
    results = {}

    ns_to_skip = set()
    clf_params_to_skip = set()

    n_hyperparams = len(hyperparameter_space)
    for index, hyperparams in enumerate(hyperparameter_space):
        error_flag = False
        if index % 100 == 0:
            logger.info('Testing hyperparameter selection {}/{}'.format(index + 1, n_hyperparams))

        if skip_broken_hyperparams and (hyperparams.n in ns_to_skip or hyperparams.clf in clf_params_to_skip):
            logger.info('Skipping broken hyperparameter configuration')
            continue

        scaled_aucs = []
        scaled_accuracies = []
        k_fold = StratifiedGroupKFold(n_splits=10)
        for train_index, test_index in k_fold.split(X, Y, groups):
            clf = classifier(**dict(hyperparams.clf))
            # get unweighted AUC and accuracy
            try:
                with warnings.catch_warnings():
                    warnings.filterwarnings('error')
                    try:
                        auc, accuracy = classification_step(clf, hyperparams, X, Y, train_index, test_index)
                    except ConvergenceWarning:
                        logger.info('Failed to converge')
                        clf_params_to_skip.add(hyperparams.clf)
                        error_flag = True
                        break

            except ValueError as e:
                if e.args[0].startswith('k should be'):
                    logger.info('k too large in feature selection')
                    ns_to_skip.add(hyperparams.n)
                    error_flag = True
                    break
                else:
                    raise e

            scaled_accuracies.append(accuracy * len(test_index))
            scaled_aucs.append(auc * len(test_index))

        if not error_flag:
            results[hyperparams] = (sum(scaled_aucs) / len(Y), sum(scaled_accuracies) / len(Y))

    return results


def do_classification(hyperparameter_space, X, Y, groups, classifier, optimise_auc=True):
    """
    Evaluate the performance of the classifier with cross-validation,
    doing hyperparameter search within each fold.
    """
    scaled_aucs = []
    scaled_accuracies = []
    k_fold = StratifiedGroupKFold(n_splits=10)
    for train_index, test_index in k_fold.split(X, Y, groups):
        logger.info('New fold')
        X_train, Y_train = X[train_index], Y[train_index]
        X_test, Y_test = X[test_index], Y[test_index]
        groups_train = groups[train_index]

        hyperparam_results = hyperparameter_search(hyperparameter_space,
                                                   X_train, Y_train, groups_train,
                                                   classifier)
        keys = list(hyperparam_results.keys())
        if optimise_auc:
            best_hyperparams = max(keys, key=lambda x: hyperparam_results[x][0])
        else:
            # optimising for accuracy
            best_hyperparams = max(keys, key=lambda x: hyperparam_results[x][1])

        clf = classifier(**best_hyperparams.clf)
        # get unweighted AUC and accuracy
        auc, accuracy = classification_step(clf, best_hyperparams, X, Y, train_index, test_index)

        scaled_accuracies.append(accuracy * len(test_index))
        scaled_aucs.append(auc * len(test_index))

    return scaled_aucs, scaled_accuracies


def explore_svms():
    """
    Do classification using SVMs with hyperparameter search as an outer loop,
    exploring hyperparameter space and selections of features.
    """
    do_scaling = False

    for include_intensities, include_expressions, include_overlaps, include_gaze in product(*((False, True),) * 4):
        if not any((include_intensities, include_expressions,
                    include_overlaps, include_gaze)):
            # must have some features
            continue

        output_logger.info('Intensities: {}, Expressions: {}, Overlaps: {}, Gaze: {}'.format(include_intensities,
                                                                                             include_expressions,
                                                                                             include_overlaps,
                                                                                             include_gaze))
        X, Y, groups, feature_names = load_features(include_intensities, include_expressions,
                                                    include_overlaps, include_gaze)

        max_n = X.shape[1]
        if max_n > 50:
            n_features = range(25, max_n, 5)
        else:
            n_features = range(1, max_n, 1)

        reg_exponents = np.arange(6, 17, 1)
        gamma_exponents = np.arange(-7, 1, 1)

        hyperparam_space = []
        for n in n_features:
            for r in reg_exponents:
                for g in gamma_exponents:
                    clf_params = (('C', 10**int(r)),
                                  ('gamma', 10**int(g)),
                                  ('kernel', 'rbf'),
                                  ('max_iter', 10**6))
                    hyperparam_space.append(Hyperparameters(clf=clf_params, n=n))

        if do_scaling:
            X = X.astype(float)
            unscaled_X = X.values
            X = preprocessing.scale(X)

        hyperparam_results = hyperparameter_search(hyperparam_space, X, Y, groups, SVC)
        results = sorted(list(hyperparam_results.keys()), key=lambda x: hyperparam_results[x][0])
        output_logger.info('Best AUCs:')
        for r in results[-25:]:
            output_logger.info('{} {}'.format(r, hyperparam_results[r]))

        results = sorted(list(hyperparam_results.keys()), key=lambda x: hyperparam_results[x][1])
        output_logger.info('Best accuracies:')
        for r in results[-25:]:
            output_logger.info('{} {}'.format(r, hyperparam_results[r]))

        output_logger.info('')

        gc.collect()


if __name__ == '__main__':
    explore_svms()
