"""
Produce pupil dilation from eye landmarks.

See https://github.com/TadasBaltrusaitis/OpenFace/wiki/Output-Format for landmark details.
"""
import os
from collections import defaultdict

import numpy as np
import pandas as pd
from scipy.stats import ttest_ind

from constants import CLIP_COUNTS, EYE_LANDMARK_FEATURES_LOCATION
from utils import sample_filename


def get_diameters(dataset):
    left_landmark_pairs = [(20, 24), (21, 25), (22, 26), (23, 27)]
    right_landmark_pairs = [(48, 52), (49, 53), (50, 54), (51, 55)]

    diameters = []
    for eye in (left_landmark_pairs, right_landmark_pairs):
        result = []
        for a, b in eye:
            diffs = []
            for axis in ('X', 'Y', 'Z'):
                a_coord = dataset['eye_lmk_{}_{}'.format(axis, a)]
                b_coord = dataset['eye_lmk_{}_{}'.format(axis, b)]
                diffs.append(a_coord - b_coord)

            diameter = np.sqrt(sum(c**2 for c in diffs))
            diameters.append(diameter)

    result = sum(diameters) / len(diameters)
    return result


if __name__ == '__main__':
    diameters = defaultdict(list)
    stds = defaultdict(list)
    maxes = defaultdict(list)
    for category in ('Deceptive', 'Truthful'):
        for i in range(1, CLIP_COUNTS[category] + 1):
            filename = sample_filename(category, i, 'csv')
            df = pd.read_csv(os.path.join(EYE_LANDMARK_FEATURES_LOCATION, filename))
            df.rename(columns=lambda x: x.strip(), inplace=True)
            diams = get_diameters(df)
            diameters[category].append(diams.mean())
            stds[category].append(diams.std())
            maxes[category].append(diams.max())

    for feature in (diameters, stds, maxes):
        print(ttest_ind(feature['Deceptive'], feature['Truthful']))
