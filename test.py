"""
Perform unit tests of components of the deception detection system.
"""
import unittest

import numpy as np

from utils import StratifiedGroupKFold


class TestStratifiedGroupCrossValidation(unittest.TestCase):
    def folds_valid(self, X, Y, groups, folds):
        """
        Assert that a set of folds is valid.

        This means the training sets and test sets should be disjoint,
        each pair of training and test sets should cover exactly all samples,
        all the test sets should collectively cover exactly all samples,
        and each group should appear in exactly one test set.
        """

        all_samples = set(range(len(X)))
        for training, test in folds:
            self.assertEqual(set(training) & set(test), set())
            self.assertEqual(set(training) | set(test), all_samples)

        self.assertEqual(set.intersection(*[set(test) for _, test in folds]), set())
        self.assertEqual(set.union(*[set(test) for _, test in folds]), all_samples)

        for g in np.unique(groups):
            count = 0
            for _, test in folds:
                if (groups[test] == g).any():
                    count += 1

            self.assertEqual(count, 1)

    def test_validation_even_split(self):
        X = np.arange(100)
        Y = np.array([0]*50 + [1]*50)        # noqa: E226
        groups = np.array([i % 10 for i in range(100)])
        k_fold = StratifiedGroupKFold(n_splits=10)
        folds = list(k_fold.split(X, Y, groups))

        self.folds_valid(X, Y, groups, folds)

    def test_validation_uneven_split(self):
        X = np.arange(100)
        Y = np.array([0]*40 + [1]*60)        # noqa: E226
        groups = np.array([i % 13 for i in range(100)])
        k_fold = StratifiedGroupKFold(n_splits=10)
        folds = list(k_fold.split(X, Y, groups))

        self.folds_valid(X, Y, groups, folds)

    def test_validation_impossible(self):
        X = np.arange(10)
        Y = np.array([0]*5 + [1]*5)        # noqa: E226
        groups = np.array([i % 5 for i in range(10)])
        k_fold = StratifiedGroupKFold(n_splits=10)

        with self.assertRaises(ValueError):
            list(k_fold.split(X, Y, groups))


if __name__ == '__main__':
    unittest.main()
